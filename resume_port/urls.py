from django.conf.urls import patterns, include, url
from django.contrib import admin
from resume_port.fapi import MyApi
from resume.views import ResumeServices

# v1 version- registering apis
v1_api = MyApi()
v1_api.register(ResumeServices())

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'resume_port.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^resume/manager/', include(v1_api.urls)),
    url(r'^oauth2/', include('provider.oauth2.urls', namespace='oauth2')),
    url(r'^admin/', include(admin.site.urls)),
)
