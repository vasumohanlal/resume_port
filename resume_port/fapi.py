from django.conf.urls import patterns, include, url
from tastypie.api import Api
from tastypie.utils import trailing_slash

__author__ = 'mohanlal'


class MyApi(Api):
    """
    An API subclass that circumvents api_name versioning.
    """

    @property
    def urls(self):
        """
        Provides URLconf details for the ``Api`` and all registered
        ``Resources`` beneath it.
        """
        pattern_list = [
            url(r"^%s$" % trailing_slash(), self.wrap_view('top_level')),
        ]

        for name in sorted(self._registry.keys()):
            # pattern_list.append((r'', include(self._registry[name].urls)))
            pattern_list.append((r'^v1/', include(self._registry[name].urls)))
        urlpatterns = self.override_urls() + patterns('', *pattern_list)
        return urlpatterns
