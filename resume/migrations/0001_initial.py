# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Resume',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('qualification', models.CharField(max_length=255)),
                ('age', models.IntegerField(default=0, choices=[(0, b'18'), (1, b'19'), (2, b'20'), (3, b'21'), (4, b'22'), (5, b'23'), (6, b'24'), (7, b'25'), (8, b'26'), (9, b'27'), (10, b'28'), (11, b'29'), (12, b'30')])),
                ('sex', models.IntegerField(default=0, choices=[(0, b''), (1, b'male'), (2, b'female')])),
                ('experience', models.IntegerField(default=0, choices=[(0, b'fresher'), (1, b'2-5'), (2, b'5-8'), (3, b'8-10'), (4, b'10+yrs')])),
                ('created_date', models.DateTimeField(default=datetime.datetime.now, auto_now_add=True)),
                ('modified_date', models.DateTimeField(default=datetime.datetime.now, auto_now=True)),
            ],
            options={
                'db_table': 'resume_info',
            },
            bases=(models.Model,),
        ),
    ]
