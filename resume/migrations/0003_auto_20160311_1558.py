# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('resume', '0002_resume_upload_resume'),
    ]

    operations = [
        migrations.AlterField(
            model_name='resume',
            name='upload_resume',
            field=models.FileField(null=True, upload_to=b'resume', blank=True),
            preserve_default=True,
        ),
    ]
