from django.contrib import admin
from models import Resume
# Register your models here.

class ResumeAdmin(admin.ModelAdmin):
    list_filter = ('sex', 'experience', 'created_date', 'modified_date')
    list_display = ('id', 'name', 'qualification', 'age', 'sex', 'upload_resume', 'experience', 'created_date')

admin.site.register(Resume, ResumeAdmin)
