import sys
import traceback

__author__ = 'mohanlal'

def print_exception():
    exp = traceback.format_exception(*sys.exc_info())
    print "\nException"
    print exp
    print "\n"
    return exp
