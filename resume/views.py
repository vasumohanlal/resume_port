import json
from django.shortcuts import render
from tastypie.utils.urls import trailing_slash
from django.conf.urls import url
from tastypie.resources import Resource
from django.template import Template, Context, RequestContext
from django.http import HttpResponse
from .commons import print_exception
from models import Resume

# Create your views here.
__author__ = 'mohanlal'

class ResumeServices(Resource):
    class Meta:
        resource_name = 'resume'

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/show%s$" % (self._meta.resource_name, trailing_slash()),
                    self.wrap_view('open_resume'), name="api_open_resume"),
            ]

    def open_resume(self, request, **kwargs):
        print "resume"
        try:
            resume_data = Resume.objects.get(id=1)
            test_data = {
                'resume_loc': resume_data.age
                }
            return HttpResponse(json.dumps({"status": "success", "data": test_data}))
        except:
            return HttpResponse(json.dumps({"status": "fail", "data": print_exception()}))

