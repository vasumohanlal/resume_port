# Python Imports
from provider.constants import READ_WRITE
from provider.oauth2.models import AccessToken
from tastypie.resources import ModelResource
import datetime

class UserResource(ModelResource):

    def get_create_access_token(self, request, user):
        print "get_create_access_token"

        try:
            access_token = AccessToken.objects.get(user=user)
            return access_token
        except:
            today = datetime.datetime.today().date()
            expiry_date = today + datetime.timedelta(days=50 * 365)
            access_token = AccessToken(user=user, client=request.client, scope=READ_WRITE, expires=expiry_date)
            access_token.save()
            return access_token
