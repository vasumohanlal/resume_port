from django.db import models
from datetime import datetime
# Create your models here.

sex = ((0, ''), (1, 'male'), (2, 'female'))
age = ((0, '18'), (1, '19'), (2, '20'), (3, '21'), (4, '22'), (5, '23'), (6, '24'), (7, '25'), (8, '26'), (9, '27'),
       (10, '28'), (11, '29'), (12, '30'))
exp = ((0, 'fresher'), (1, '2-5'), (2, '5-8'), (3, '8-10'), (4, '10+yrs'))

class Resume(models.Model):
    name = models.CharField(max_length=255, blank=False, null=False)
    qualification = models.CharField(max_length=255, blank=False, null=False)
    age = models.IntegerField(default=0, choices=age)
    sex = models.IntegerField(default=0, choices=sex)
    experience = models.IntegerField(default=0, choices=exp)
    upload_resume = models.FileField(upload_to='resume', blank=True, null=True)
    created_date = models.DateTimeField(default=datetime.now, auto_now_add=True, blank=True)
    modified_date = models.DateTimeField(default=datetime.now, auto_now=True, blank=True)

    class Meta:
        db_table = 'resume_info'
